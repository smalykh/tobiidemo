Quck Setup

1. Download and install Tobii core SDK from here https://gaming.tobii.com/getstarted/?bundle=tobii-core
2. Download and install Visual Studio
3. Add the Tobii Core SDK package by:
      
      Right click on the project's reference folder and choose Manage NuGet Packages... from the menu.
      
      Browse for the Tobii.Interaction package and click install. The project shall now be referencing Tobii.Interaction.Model.dll and Tobii.Interaction.Net.dll.
      
4. Open .sln and configure the build architecture to x86 to prevent potential warnings and build errors.
5. Plug Tobii Eye Tracker and run application!

For more details, please, visit https://developer.tobii.com/consumer-eye-trackers/core-sdk/getting-started/