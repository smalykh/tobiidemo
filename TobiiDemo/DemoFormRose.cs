﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TobiiDemo
{
    public partial class DemoFormRose : DemoForm
    {
        public DemoFormRose()
        {
            InitializeComponent();
            Extention = new DemoFormExtension(
                            this,
                            /* Corresponding with Words { OFF, IDLE, FORWARD, LEFT, RIGHT, FORWARD_LEFT, FORWARD_RIGHT }; */
                            new Button[] {
                                buttonDown,
                                buttonCenter,
                                buttonUp,
                                buttonLeft,
                                buttonRight,
                                buttonUpLeft,
                                buttonUpRight
                            },
                            buttonCenter
                        );
        }

        public override void CustomizeDemoForm(int thX, int thY)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            int[] h = { thY, 0, thY };
            int[] w = { thX, 0, thX };

            tableLayoutPanel.RowStyles[0].SizeType = SizeType.Absolute;
            tableLayoutPanel.RowStyles[0].Height = thY;
            tableLayoutPanel.RowStyles[2].SizeType = SizeType.Absolute;
            tableLayoutPanel.RowStyles[2].Height = thY;

            tableLayoutPanelUp.ColumnStyles[0].SizeType = SizeType.Absolute;
            tableLayoutPanelUp.ColumnStyles[0].Width = thX;
            tableLayoutPanelUp.ColumnStyles[2].SizeType = SizeType.Absolute;
            tableLayoutPanelUp.ColumnStyles[2].Width = thX;


            tableLayoutPanelMiddle.ColumnStyles[0].SizeType = SizeType.Absolute;
            tableLayoutPanelMiddle.ColumnStyles[0].Width = thX;
            tableLayoutPanelMiddle.ColumnStyles[2].SizeType = SizeType.Absolute;
            tableLayoutPanelMiddle.ColumnStyles[2].Width = thX;

        }

        private void DemoForm_Load(object sender, EventArgs e)
        {
        }
    }
}
