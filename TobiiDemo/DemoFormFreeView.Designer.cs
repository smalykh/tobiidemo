﻿using System.Windows;
using System;
using System.Windows.Forms;
namespace TobiiDemo
{
    partial class DemoFormFreeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoFormFreeView));
            this.label1 = new System.Windows.Forms.Label();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.buttonRigth = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonCenter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(71, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // buttonUp
            // 
            this.buttonUp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonUp.Location = new System.Drawing.Point(71, 0);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(649, 23);
            this.buttonUp.TabIndex = 1;
            this.buttonUp.Text = "Start navigation";
            this.buttonUp.UseVisualStyleBackColor = true;
            // 
            // buttonLeft
            // 
            this.buttonLeft.AutoSize = true;
            this.buttonLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonLeft.Location = new System.Drawing.Point(0, 0);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(71, 450);
            this.buttonLeft.TabIndex = 2;
            this.buttonLeft.Text = "Turn left";
            this.buttonLeft.UseVisualStyleBackColor = true;
            // 
            // buttonRigth
            // 
            this.buttonRigth.AutoSize = true;
            this.buttonRigth.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonRigth.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonRigth.Location = new System.Drawing.Point(720, 0);
            this.buttonRigth.Name = "buttonRigth";
            this.buttonRigth.Size = new System.Drawing.Size(80, 450);
            this.buttonRigth.TabIndex = 3;
            this.buttonRigth.Text = "Turn rigth";
            this.buttonRigth.UseVisualStyleBackColor = true;
            // 
            // buttonDown
            // 
            this.buttonDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonDown.Location = new System.Drawing.Point(71, 427);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(649, 23);
            this.buttonDown.TabIndex = 4;
            this.buttonDown.Text = "Stop Navigation";
            this.buttonDown.UseVisualStyleBackColor = true;
            // 
            // buttonCenter
            // 
            this.buttonCenter.AutoSize = true;
            this.buttonCenter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCenter.Image = ((System.Drawing.Image)(resources.GetObject("buttonCenter.Image")));
            this.buttonCenter.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonCenter.Location = new System.Drawing.Point(71, 23);
            this.buttonCenter.Name = "buttonCenter";
            this.buttonCenter.Size = new System.Drawing.Size(649, 404);
            this.buttonCenter.TabIndex = 5;
            this.buttonCenter.Text = "Move forward";
            this.buttonCenter.UseVisualStyleBackColor = true;
            // 
            // DemoFormFreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonCenter);
            this.Controls.Add(this.buttonDown);
            this.Controls.Add(this.buttonUp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonRigth);
            this.Controls.Add(this.buttonLeft);
            this.Name = "DemoFormFreeView";
            this.Text = "DemoForm";
            this.Load += new System.EventHandler(this.DemoForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonLeft;
        private System.Windows.Forms.Button buttonRigth;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonCenter;
    }
}