﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TobiiDemo
{
    public partial class DemoFormAuto : DemoForm
    {
        public DemoFormAuto()
        {
            InitializeComponent();
            Extention = new DemoFormExtension(
                            this,
                            /* Corresponding with Words { OFF, ON, FORWARD, LEFT, RIGHT } */
                            new Button[] {
                                buttonStop,
                                buttonStart,
                                buttonCenter,
                                buttonLeft,
                                buttonRight
                            },
                            buttonCenter
                        );
        }

        public override void CustomizeDemoForm(int thX, int thY)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;

            tableLayoutPanel.RowStyles[1].SizeType = SizeType.Absolute;
            tableLayoutPanel.RowStyles[1].Height = thY;

            tableLayoutPanelMiddle.ColumnStyles[0].SizeType = SizeType.Absolute;
            tableLayoutPanelMiddle.ColumnStyles[0].Width = thX;
            tableLayoutPanelMiddle.ColumnStyles[2].SizeType = SizeType.Absolute;
            tableLayoutPanelMiddle.ColumnStyles[2].Width = thX;

        }

        private void DemoForm_Load(object sender, EventArgs e)
        {
        }
    }
}
