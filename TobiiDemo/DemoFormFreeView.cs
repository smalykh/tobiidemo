﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TobiiDemo
{
    public partial class DemoFormFreeView : DemoForm
    {
        public DemoFormFreeView()
        {
            InitializeComponent();
            Extention = new DemoFormExtension(
                            this, 
                            /* Corresponding with Words { OFF, ON, FORWARD, LEFT, RIGHT } */
                            new Button[] {
                                buttonDown,
                                buttonUp,
                                buttonCenter,
                                buttonLeft,
                                buttonRigth 
                            },
                            buttonCenter
                        );
        }

        public override void CustomizeDemoForm(int thX, int thY)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;

            this.buttonUp.MinimumSize = new System.Drawing.Size(0, thY);
            this.buttonDown.MinimumSize = new System.Drawing.Size(0, thY);
            this.buttonLeft.MinimumSize = new System.Drawing.Size(thX, 0);
            this.buttonRigth.MinimumSize = new System.Drawing.Size(thX, 0);
        }

        private void DemoForm_Load(object sender, EventArgs e)
        {
        }
    }
}
