﻿using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using Tobii.Interaction;


namespace TobiiDemo
{
    abstract class CommandAdapter
    {
        public enum MoveCommandType { StopCommand, ForwardCommand, TurnLeftCommand, TurnRigthCommand, ForwardLeftCommand, ForwardRigthCommand };
        public abstract int SendCommand(MoveCommandType cmd);
    }

    class MoveCommandAdapter : CommandAdapter
    {
        Socket Sender;

        public override int SendCommand(MoveCommandType cmd)
        {
            Console.WriteLine("Send {0}", cmd.ToString());
            switch (cmd)
            {
                case MoveCommandType.StopCommand:
                    Sender.Send(Encoding.ASCII.GetBytes("s"));
                    break;
                case MoveCommandType.ForwardCommand:
                    Sender.Send(Encoding.ASCII.GetBytes("w"));
                    break;
                case MoveCommandType.TurnLeftCommand:
                    Sender.Send(Encoding.ASCII.GetBytes("a"));
                    break;
                case MoveCommandType.TurnRigthCommand:
                    Sender.Send(Encoding.ASCII.GetBytes("d"));
                    break;
                case MoveCommandType.ForwardLeftCommand:
                    Sender.Send(Encoding.ASCII.GetBytes("q"));
                    break;
                case MoveCommandType.ForwardRigthCommand:
                    Sender.Send(Encoding.ASCII.GetBytes("e"));
                    break;
            }
            return 0;
        }

        public MoveCommandAdapter(IPAddress ipAddr, int port)
        {
            IPEndPoint localEndPoint = new IPEndPoint(ipAddr, port);
            Sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            Sender.Connect(localEndPoint);
        }
    }

    class DemoCommandAdapter : CommandAdapter
    {
        public EventHandler OnMoveCommandReceived;

        private DemoForm Form;
        public override int SendCommand(MoveCommandType cmd)
        {
            OnMoveCommandReceived?.Invoke(cmd, null);
            return 0;
        }

        public DemoCommandAdapter(DemoForm Form) : base()
        {
            this.Form = Form;
        }
    }

    abstract class FieldSeparator
    {
        /* Notify when new field in focus */
        public EventHandler OnCommandReceived;
        abstract public int FieldByXY(double x, double y);
        abstract public int GetDemoForm(double x, double y);

    }

    abstract class StateMachine
    {
        private int state;
        protected int State
        {
            get { return state; }
            set { state = value; OnStateChanged?.Invoke(state, null); }
        }
        DateTimeOffset RecentStateTransitionTime;
        TimeSpan SelectorLatency;

        public const int LatencySec = 0, LatencyMillisec = 500;

        /* Notify when system status (off / on / continuous forward) is changed */
        public EventHandler OnStateChanged;

        protected void UpdateStateTransitionTime()
        {
            RecentStateTransitionTime = DateTimeOffset.Now;
        }

        protected bool IsStateStable()
        {
            return (DateTimeOffset.Now - RecentStateTransitionTime) > SelectorLatency;
        }

        protected CommandAdapter CommandAdapter;
        abstract public void Process(int v);
        abstract public void Reset();

        protected StateMachine(CommandAdapter CommandAdapter)
        {
            this.CommandAdapter = CommandAdapter;
            SelectorLatency = new TimeSpan(0, 0, 0, LatencySec, LatencyMillisec);
        }
    }

    class AutoFieldSeparator : FieldSeparator
    {
        public enum Words { OFF, ON, FORWARD, LEFT, RIGHT };

        static double BoundThresholdRatioX = 0.2;
        static double BoundThresholdRatioY = 0.1;

        int ResolutionX, ResolutionY;
        int BoundThresholdX, BoundThresholdY;

        public override int FieldByXY(double x, double y)
        {
            Words word;

            if (y > ResolutionY - BoundThresholdY)
            {
                if (x > ResolutionX / 2)
                    word = Words.ON;
                else
                    word = Words.OFF;
            }
            else
            {
                if (x < BoundThresholdX)
                    word = Words.LEFT;
                else if (x > ResolutionX - BoundThresholdX)
                    word = Words.RIGHT;
                else
                    word = Words.FORWARD;
            }

            OnCommandReceived?.Invoke(word, null);
            return (int)word;
        }

        public override int GetDemoForm(double x, double y)
        {
            throw new NotImplementedException();
        }

        public AutoFieldSeparator()
        {
            ResolutionY = Screen.PrimaryScreen.Bounds.Height;
            ResolutionX = Screen.PrimaryScreen.Bounds.Width;

            BoundThresholdX = (int)(BoundThresholdRatioX * ResolutionX);
            BoundThresholdY = (int)(BoundThresholdRatioY * ResolutionY);
        }
    }

    class FreeFieldSeparator : FieldSeparator
    {
        public enum Words { OFF, ON, FORWARD, LEFT, RIGHT };

        static double BoundThresholdRatioX = 0.2;
        static double BoundThresholdRatioY = 0.1;

        int ResolutionX, ResolutionY;
        int BoundThresholdX, BoundThresholdY;

        public override int FieldByXY(double x, double y)
        {
            Words word = Words.FORWARD;

            if (x < BoundThresholdX)
                word = Words.LEFT;
            else if (x > ResolutionX - BoundThresholdX)
                word = Words.RIGHT;
            else if (y < BoundThresholdY)
                word = Words.ON;
            else if (y > ResolutionY - BoundThresholdY)
                word = Words.OFF;


            OnCommandReceived?.Invoke(word, null);
            return (int)word;
        }

        public override int GetDemoForm(double x, double y)
        {
            throw new NotImplementedException();
        }

        public FreeFieldSeparator()
        {
            ResolutionY = Screen.PrimaryScreen.Bounds.Height;
            ResolutionX = Screen.PrimaryScreen.Bounds.Width;

            BoundThresholdX = (int)(BoundThresholdRatioX * ResolutionX);
            BoundThresholdY = (int)(BoundThresholdRatioY * ResolutionY);
        }
    }

    class FreeStateMachine : StateMachine
    {
        public enum States { OFF, IDLE0, IDLE1, FORWARD, LEFT, RIGHT, CONTINUOUS_FORWARD, CONTINUOUS_LEFT, CONTINUOUS_RIGHT };
        public enum Words { OFF, ON, FORWARD, LEFT, RIGHT };

        private States mState
        {
            set
            {
                if (State != (int)value)
                {
                    Console.WriteLine("System state: " + value.ToString());
                    OnStateChanged?.Invoke(value, null);
                }
                State = (int)value;
            }
            get { return (States)State; }

        }

        public override void Process(int v)
        {
            Words word = (Words)v;

            switch (mState)
            {
                case States.OFF:
                    if (IsStateStable() && (word == Words.ON))
                    {
                        UpdateStateTransitionTime();
                        mState = States.IDLE0;
                    }
                    break;

                case States.IDLE0:
                    if (word != Words.ON)
                    {
                        mState = States.IDLE1;
                        goto case States.IDLE1;
                    }
                    break;

                case States.IDLE1:
                    /* Got a reason to think about changing of direction is just noise.
                     * Stop Engine and wait while State become stable.*/
                    if (!IsStateStable())
                        break;

                    switch (word)
                    {
                        case Words.OFF:
                            mState = States.OFF;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                            break;
                        case Words.ON:
                            mState = States.CONTINUOUS_FORWARD;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardCommand);
                            break;
                        case Words.FORWARD:
                            mState = States.FORWARD;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardCommand);
                            break;
                        case Words.LEFT:
                            mState = States.LEFT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.TurnLeftCommand);
                            break;
                        case Words.RIGHT:
                            mState = States.RIGHT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.TurnRigthCommand);
                            break;
                    }
                    break;

                case States.CONTINUOUS_FORWARD:
                    switch (word)
                    {
                        case Words.OFF:
                            mState = States.OFF;
                            UpdateStateTransitionTime();
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                            break;
                        case Words.LEFT:
                            mState = States.CONTINUOUS_LEFT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardLeftCommand);
                            break;
                        case Words.RIGHT:
                            mState = States.CONTINUOUS_RIGHT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardRigthCommand);
                            break;
                    }
                    break;

                case States.FORWARD:
                    if (word != Words.FORWARD)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.IDLE1;
                    }
                    break;

                case States.LEFT:
                    if (word != Words.LEFT)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.IDLE1;
                    }
                    break;

                case States.RIGHT:
                    if (word != Words.RIGHT)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.IDLE1;
                    }
                    break;

                case States.CONTINUOUS_LEFT:
                    if (word == Words.OFF)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.OFF;
                    }
                    else if (word != Words.LEFT)
                    {
                        mState = States.CONTINUOUS_FORWARD;
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardCommand);
                    }
                    break;

                case States.CONTINUOUS_RIGHT:
                    if (word == Words.OFF)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.OFF;
                    }
                    else if (word != Words.RIGHT)
                    {
                        mState = States.CONTINUOUS_FORWARD;
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardCommand);
                    }
                    break;
            }
        }

        public override void Reset()
        {
            mState = States.OFF;
            UpdateStateTransitionTime();
            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
        }
        public FreeStateMachine(CommandAdapter CommandAdapter) : base(CommandAdapter)
        {
            mState = States.OFF;
        }
    }

    class RoseFieldSeparator : FieldSeparator
    {
        public enum Words { OFF, IDLE, FORWARD, LEFT, RIGHT, FORWARD_LEFT, FORWARD_RIGHT };

        static double BoundThresholdRatioX = 0.2;
        static double BoundThresholdRatioY = 0.1;

        int ResolutionX, ResolutionY;
        int BoundThresholdX, BoundThresholdY;

        public override int FieldByXY(double x, double y)
        {
            Words word = Words.IDLE;

            if (x < BoundThresholdX)
            {
                if (y > BoundThresholdY)
                    word = Words.LEFT;
                else
                    word = Words.FORWARD_LEFT;
            }
            else if (x > ResolutionX - BoundThresholdX)
            {
                if (y > BoundThresholdY)
                    word = Words.RIGHT;
                else
                    word = Words.FORWARD_RIGHT;
            }
            else if (y < BoundThresholdY)
                word = Words.FORWARD;
            else if (y > ResolutionY - BoundThresholdY)
                word = Words.OFF;

            OnCommandReceived?.Invoke(word, null);
            return (int)word;
        }

        public override int GetDemoForm(double x, double y)
        {
            throw new NotImplementedException();
        }

        public RoseFieldSeparator()
        {
            ResolutionY = Screen.PrimaryScreen.Bounds.Height;
            ResolutionX = Screen.PrimaryScreen.Bounds.Width;

            BoundThresholdX = (int)(BoundThresholdRatioX * ResolutionX);
            BoundThresholdY = (int)(BoundThresholdRatioY * ResolutionY);
        }
    }

    class RoseStateMachine : StateMachine
    {
        public enum States { OFF0, OFF1, IDLE0, IDLE, FORWARD, LEFT, RIGHT, FORWARD_LEFT, FORWARD_RIGHT };
        public enum Words { OFF, IDLE, FORWARD, LEFT, RIGHT, FORWARD_LEFT, FORWARD_RIGHT };
        private States mState
        {
            set
            {
                if (State != (int)value)
                {
                    Console.WriteLine("System state: " + value.ToString());
                    OnStateChanged?.Invoke(value, null);
                }
                State = (int)value;
            }
            get { return (States)State; }

        }

        public override void Process(int v)
        {
            Words word = (Words)v;

            switch (mState)
            {
                case States.OFF0:
                    if (IsStateStable() && (word == Words.OFF))
                    {
                        mState = States.IDLE0;
                    }
                    break;

                case States.OFF1:
                    if (word != Words.OFF)
                    {
                        UpdateStateTransitionTime();
                        mState = States.OFF0;
                    }
                    break;

                case States.IDLE0:
                    if (word != Words.OFF)
                    {
                        UpdateStateTransitionTime();
                        mState = States.IDLE;
                        goto case States.IDLE;
                    }
                    break;
                case States.IDLE:
                    /* Got a reason to think about changing of direction is just noise.
                     * Stop Engine and wait while State become stable.*/
                    if (!IsStateStable())
                        break;

                    switch (word)
                    {
                        case Words.OFF:
                            mState = States.OFF1;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                            break;
                        case Words.FORWARD:
                            mState = States.FORWARD;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardCommand);
                            break;
                        case Words.LEFT:
                            mState = States.LEFT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.TurnLeftCommand);
                            break;
                        case Words.RIGHT:
                            mState = States.RIGHT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.TurnRigthCommand);
                            break;
                        case Words.FORWARD_LEFT:
                            mState = States.FORWARD_LEFT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardLeftCommand);
                            break;
                        case Words.FORWARD_RIGHT:
                            mState = States.FORWARD_RIGHT;
                            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.ForwardRigthCommand);
                            break;
                    }
                    break;

                case States.FORWARD:
                    if (word != Words.FORWARD)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.IDLE;
                    }
                    break;

                case States.LEFT:
                    if (word != Words.LEFT)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.IDLE;
                    }
                    break;

                case States.RIGHT:
                    if (word != Words.RIGHT)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.IDLE;
                    }
                    break;

                case States.FORWARD_LEFT:
                    if (word != Words.FORWARD_LEFT)
                    {
                        UpdateStateTransitionTime();
                        mState = States.IDLE;
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                    }
                    break;

                case States.FORWARD_RIGHT:
                    if (word != Words.FORWARD_RIGHT)
                    {
                        UpdateStateTransitionTime();
                        CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
                        mState = States.IDLE;
                    }
                    break;
            }
        }

        public override void Reset()
        {
            mState = States.OFF0;
            UpdateStateTransitionTime();
            CommandAdapter.SendCommand(CommandAdapter.MoveCommandType.StopCommand);
        }
        public RoseStateMachine(CommandAdapter CommandAdapter) : base(CommandAdapter)
        {
            mState = States.OFF0;
        }
    }

    class TobiiWatchdog
    {
        /* Trap gaps in tobii data flow */
        System.Timers.Timer Watchdog;
        /* Every 1000 ms gaze point update expected */
        const int TobiiWatchdogPeriod = 1000;
        /* To know how much time silence on channel */
        DateTimeOffset RecentCommandTime;
        /* Object-owner of this object */
        Interpreter Parent;

        private void OnTobiiWatchdog(object source, ElapsedEventArgs e)
        {
            lock (Parent)
            {
                TimeSpan SilenceTime = DateTimeOffset.Now - RecentCommandTime;
                TimeSpan MaxSilenceTIme = new TimeSpan(0, 0, 0, 0, TobiiWatchdogPeriod);
                if (SilenceTime < MaxSilenceTIme)
                {
                    /* False positive alarm */
                    return;
                }

                /* Send stop and wait for next commands from Tobii */
                Parent.Reset();

            }
        }

        public void Update()
        {
            RecentCommandTime = DateTimeOffset.Now;
            Watchdog.Stop();
            Watchdog.Start();
        }

        public TobiiWatchdog(Interpreter parent)
        {
            Watchdog = new System.Timers.Timer(TobiiWatchdogPeriod);
            Watchdog.Elapsed += new ElapsedEventHandler(OnTobiiWatchdog);
            Watchdog.AutoReset = false;
            Watchdog.Enabled = false;
            Parent = parent;
        }

    }

    class Interpreter
    {
        TobiiWatchdog TobiiWatchdog;
        public StateMachine StateMachine;
        public FieldSeparator FieldSeparator;

        public void Reset()
        {
            lock (this)
            {
                StateMachine.Reset();
            }
        }

        public void Handler(double x, double y)
        {
            lock (this)
            {
                TobiiWatchdog.Update();
                int v = FieldSeparator.FieldByXY(x, y);
                StateMachine.Process(v);
            }
        }

        public Interpreter(FieldSeparator FieldSeparator, StateMachine StateMachine)
        {
            TobiiWatchdog = new TobiiWatchdog(this);
            this.FieldSeparator = FieldSeparator;
            this.StateMachine = StateMachine;
        }
    }

    class EventArgsDemo : EventArgs
    {
        public string Message;
        public EventArgsDemo(string msg)
        {
            Message = msg;
        }
    }

    class DemoEngine
    {
        public EventHandler OnUserAction;
        int UserProgress = 0; /* User-incremented index connected with StateSequence */

        int[] StateSequence;
        int[] Sequence;
        string[] Messages;

        public void StateChangedCallback(object sender, EventArgs e)
        {
            int UserAction = (int)sender;

            if (UserProgress >= StateSequence.Length)
                return;
            if (StateSequence[UserProgress] == UserAction)
            {
                UserProgress++;

                if (UserProgress >= StateSequence.Length)
                    return;

                OnUserAction?.Invoke(Sequence[UserProgress], new EventArgsDemo(Messages[UserProgress]));
            }
        }

        public void StartDemoEngine()
        {
            /* Publish first task by user */
            OnUserAction?.Invoke(StateSequence[UserProgress], new EventArgsDemo(Messages[UserProgress]));
        }

        public DemoEngine(Program.InterpreterType type)
        {
            switch (type)
            {
                case Program.InterpreterType.FreeView:
                    goto case Program.InterpreterType.Auto;
                case Program.InterpreterType.Auto:
                    StateSequence = new int[] {
                        (int)FreeStateMachine.States.IDLE0,
                        (int)FreeStateMachine.States.IDLE1,
                        (int)FreeStateMachine.States.FORWARD,
                        (int)FreeStateMachine.States.LEFT,
                        (int)FreeStateMachine.States.FORWARD,
                        (int)FreeStateMachine.States.RIGHT,
                        (int)FreeStateMachine.States.FORWARD,
                        (int)FreeStateMachine.States.CONTINUOUS_FORWARD,
                        (int)FreeStateMachine.States.CONTINUOUS_LEFT,
                        (int)FreeStateMachine.States.CONTINUOUS_RIGHT,
                        (int)FreeStateMachine.States.OFF
                    };

                    Sequence = new int[] {
                        (int)FreeFieldSeparator.Words.ON,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.LEFT,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.RIGHT,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.ON,
                        (int)FreeFieldSeparator.Words.LEFT,
                        (int)FreeFieldSeparator.Words.RIGHT,
                        (int)FreeFieldSeparator.Words.OFF
                    };

                    Messages = new string[] {
                        "Let's turn on navigation",
                        "Move eye in any other field",
                        "Let's move forward",
                        "Let's turn left", "Let's move forward",
                        "Let's turn right", "Let's move forward",
                        "Let's activate continuous forward mode",
                        "Let's move left-forward",
                        "Let's move right-forward",
                        "Let's stop nagivation",
                        "Ok! Free running mode!",
                    };
                    break;
                case Program.InterpreterType.Rose:
                    StateSequence = new int[] {
                        (int)FreeStateMachine.States.IDLE0,
                        (int)FreeStateMachine.States.IDLE1,
                        (int)FreeStateMachine.States.FORWARD,
                        (int)FreeStateMachine.States.LEFT,
                        (int)FreeStateMachine.States.FORWARD,
                        (int)FreeStateMachine.States.RIGHT,
                        (int)FreeStateMachine.States.FORWARD,
                        (int)FreeStateMachine.States.CONTINUOUS_FORWARD,
                        (int)FreeStateMachine.States.CONTINUOUS_LEFT,
                        (int)FreeStateMachine.States.CONTINUOUS_RIGHT,
                        (int)FreeStateMachine.States.OFF
                    };

                    Sequence = new int[] {
                        (int)FreeFieldSeparator.Words.ON,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.LEFT,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.RIGHT,
                        (int)FreeFieldSeparator.Words.FORWARD,
                        (int)FreeFieldSeparator.Words.ON,
                        (int)FreeFieldSeparator.Words.LEFT,
                        (int)FreeFieldSeparator.Words.RIGHT,
                        (int)FreeFieldSeparator.Words.OFF
                    };

                    Messages = new string[] {
                        "Let's turn on navigation",
                        "Move eye in any other field",
                        "Let's move forward",
                        "Let's turn left",
                        "Let's turn right",
                        "Let's move left-forward",
                        "Let's move right-forward",
                        "Let's stop nagivation",
                        "Ok! Free running mode!",
                    };

                    break;
            }
        }
    }

    class Beep
    {
        public static void beep(object sender, EventArgs e)
        {
            //NavigationSystem.NavigationSystemState State = (NavigationSystem.NavigationSystemState)sender;
            //if (State == NavigationSystem.NavigationSystemState.SystemStarting)
            //    System.Media.SystemSounds.Beep.Play();
            //else if (State == NavigationSystem.NavigationSystemState.SystemStopped)
            //    System.Media.SystemSounds.Hand.Play();
            //else
            //    System.Media.SystemSounds.Beep.Play();

        }
    }

    class Program
    {
        static int ResolutionX = 1366;
        static int ResolutionY = 768;
        static double BoundThresholdRatioX = 0.2;
        static double BoundThresholdRatioY = 0.1;

        static byte[] ip = new byte[] { 127, 0, 0, 1 };
        static int port = 8080;

        public enum ProgramMode { None, Demo, Navigation };
        public static ProgramMode OperatingMode = ProgramMode.None;

        public enum InterpreterType { Rose, Auto, FreeView };
        public static InterpreterType CheckedInterpreter = InterpreterType.Rose;

        static Interpreter CreateInterpreter(InterpreterType type, CommandAdapter CommandAdapter)
        {
            FieldSeparator FieldSeparator = null;
            StateMachine StateMachine = null;

            switch (type) {
                case InterpreterType.Rose:
                    FieldSeparator = new RoseFieldSeparator();
                    StateMachine = new RoseStateMachine(CommandAdapter);
                    break;
                case InterpreterType.Auto:
                    FieldSeparator = new AutoFieldSeparator();
                    StateMachine = new FreeStateMachine(CommandAdapter);
                    break;
                case InterpreterType.FreeView:
                    FieldSeparator = new FreeFieldSeparator();
                    StateMachine = new FreeStateMachine(CommandAdapter);
                    break;
            }

            return new Interpreter(FieldSeparator, StateMachine);
        }
        
        static void Main(string[] args)
        {

            ResolutionY = Screen.PrimaryScreen.Bounds.Height;
            ResolutionX = Screen.PrimaryScreen.Bounds.Width;

            int BoundThresholdX = (int)(BoundThresholdRatioX * ResolutionX);
            int BoundThresholdY = (int)(BoundThresholdRatioY * ResolutionY);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ModeSelectorForm());

            switch (OperatingMode)
            {
                case ProgramMode.Demo:
                    {
                        Control Ctl = new Control();
                        DemoEngine DemoEngine = new DemoEngine(CheckedInterpreter);
                        DemoForm Demo = null;
                        switch (CheckedInterpreter)
                        {
                            case InterpreterType.Rose:
                                Demo = new DemoFormRose();
                                break;
                            case InterpreterType.Auto:
                                Demo = new DemoFormAuto();
                                break;
                            case InterpreterType.FreeView:
                                Demo = new DemoFormFreeView();
                                break;
                        }

                        Demo.CustomizeDemoForm(BoundThresholdX, BoundThresholdY);
                        var host = new Host();
                        var commandAdapter = new DemoCommandAdapter(Demo);

                        var Interpreter = CreateInterpreter(CheckedInterpreter, commandAdapter);

                        var gazePointDataStream = host.Streams.CreateGazePointDataStream();

                        Interpreter.FieldSeparator.OnCommandReceived += new EventHandler(Demo.Extention.OnCommandReceived);
                        commandAdapter.OnMoveCommandReceived += new EventHandler(Demo.Extention.OnMoveCommandReceived);

                        Interpreter.StateMachine.OnStateChanged += new EventHandler(DemoEngine.StateChangedCallback);
                        DemoEngine.OnUserAction += new EventHandler(Demo.Extention.OnUserAction);

                        DemoEngine.StartDemoEngine();
                        gazePointDataStream.GazePoint((gazePointX, gazePointY, _) => Interpreter.Handler(gazePointX, gazePointY));
                        Application.Run(Demo);
                        break;
                    }
                case ProgramMode.Navigation:
                    {
                        var host = new Host();
                        var commandAdapter = new MoveCommandAdapter(new IPAddress(ip), port);

                        // StateMachine.OnStateChanged += new EventHandler(Beep.beep);
                        var Interpreter = CreateInterpreter(CheckedInterpreter, commandAdapter);

                        var gazePointDataStream = host.Streams.CreateGazePointDataStream();
                        gazePointDataStream.GazePoint((gazePointX, gazePointY, _) => Interpreter.Handler(gazePointX, gazePointY));
                        while (true) { Thread.Sleep(1000); }
                    }
                case ProgramMode.None:
                    return;
            }
        }
    }
}
