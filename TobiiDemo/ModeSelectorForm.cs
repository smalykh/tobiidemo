﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TobiiDemo
{
    public partial class ModeSelectorForm : Form
    {
        public ModeSelectorForm()
        {
            InitializeComponent();
        }

        private void RunDemoButton_Click(object sender, EventArgs e)
        {
            Program.OperatingMode = Program.ProgramMode.Demo;
            this.Close();
        }

        private void RunNavigationButton_Click(object sender, EventArgs e)
        {
            Program.OperatingMode = Program.ProgramMode.Navigation;
            this.Close();
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Program.CheckedInterpreter = Program.InterpreterType.Auto;
        }

        private void RadioButtonRose_CheckedChanged(object sender, EventArgs e)
        {
            Program.CheckedInterpreter = Program.InterpreterType.Rose;
        }

        private void FreeViewButton_CheckedChanged(object sender, EventArgs e)
        {
            Program.CheckedInterpreter = Program.InterpreterType.FreeView;
        }
    }
}
