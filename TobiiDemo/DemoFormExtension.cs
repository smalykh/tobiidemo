﻿using System.Windows;
using System;
using System.Windows.Forms;

namespace TobiiDemo
{
    public class DemoFormExtension
    {
        private Form Parent;
        private Button[] Buttons;
        private Button Control;

        private string SystemOn = "Navigation mode is off";
        private string DemoMsg = "";

        private System.Drawing.Color[] ButtonColors;

        public void OnUserAction(object sender, System.EventArgs e)
        {
            if (Parent.InvokeRequired)
            {
                Parent.BeginInvoke(new MethodInvoker(delegate
                {
                    OnUserAction(sender, e);
                }));
            }
            else
            {
                int state = (int)sender;

                for (int i = 0; i < Buttons.Length; i++)
                    ButtonColors[i] = System.Drawing.Color.Empty;

                ButtonColors[state] = System.Drawing.Color.Violet;

                //switch (state)
                //{
                //    case DemoEngine.DemoEngineState.Started:
                //        ButtonColors[1] = System.Drawing.Color.Violet;
                //        break;
                //    case DemoEngine.DemoEngineState.Stopped:
                //        ButtonColors[2] = System.Drawing.Color.Violet;
                //        break;
                //    case DemoEngine.DemoEngineState.Forward:
                //        ButtonColors[0] = System.Drawing.Color.Violet;
                //        break;
                //    case DemoEngine.DemoEngineState.Left:
                //        ButtonColors[3] = System.Drawing.Color.Violet;
                //        break;
                //    case DemoEngine.DemoEngineState.Rigth:
                //        ButtonColors[4] = System.Drawing.Color.Violet;
                //        break;
                //}
                DemoMsg = ((EventArgsDemo)e).Message;

                UpdateInformationText();
                ResetButtonsColor();
            }
        }

        public void OnStateChanged(object sender, System.EventArgs e)
        {

            if (Parent.InvokeRequired)
            {
                Parent.BeginInvoke(new MethodInvoker(delegate
                {
                    OnStateChanged(sender, e);
                }));
            }
            else
            {
                //NavigationSystem.NavigationSystemState State = (NavigationSystem.NavigationSystemState)sender;
                //if (State == NavigationSystem.NavigationSystemState.SystemStarted)
                //{
                //    SystemOn = "Navigation mode is on";
                //}
                //else
                //{
                //    SystemOn = "Navigation mode is off";
                //}
                UpdateInformationText();
            }
        }

        public void OnMoveCommandReceived(object sender, System.EventArgs e)
        {
            Parent.BeginInvoke(new MethodInvoker(delegate
            {
                CommandAdapter.MoveCommandType cmd = (CommandAdapter.MoveCommandType)sender;
                switch (cmd)
                {
                    case CommandAdapter.MoveCommandType.ForwardCommand:
                        SetForwardArrowVisible();
                        break;
                    case CommandAdapter.MoveCommandType.TurnLeftCommand:
                        SetLeftArrowVisible();
                        break;
                    case CommandAdapter.MoveCommandType.TurnRigthCommand:
                        SetRightArrowVisible();
                        break;
                    case CommandAdapter.MoveCommandType.ForwardLeftCommand:
                        SetForwardLeftArrowVisible();
                        break;
                    case CommandAdapter.MoveCommandType.ForwardRigthCommand:
                        SetForwardRightArrowVisible();
                        break;
                default:
                        SetArrowUnvisible();
                        break;
                }
            }));
        }

        public void OnCommandReceived(object sender, System.EventArgs e)
        {
            Parent.BeginInvoke(new MethodInvoker(delegate
            {
                int Field = (int)sender;
                ResetButtonsColor();
                Buttons[Field].BackColor = System.Drawing.Color.Yellow;
            }));
        }

        private void ResetButtonsColor()
        {
            for (int i = 0; i < Buttons.Length; i++)
                Buttons[i].BackColor = ButtonColors[i];
        }

        public void UpdateInformationText()
        {
            Control.Text = DemoMsg + "\n\n" + SystemOn;
        }

        public void SetForwardArrowVisible()
        {
            Control.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_fwd.png"));
        }

        public void SetLeftArrowVisible()
        {
            Control.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_l.png"));
        }

        public void SetRightArrowVisible()
        {
            Control.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_r.png"));
        }
        public void SetForwardLeftArrowVisible()
        {
            Control.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_fl.png"));
        }

        public void SetForwardRightArrowVisible()
        {
            Control.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_fr.png"));
        }

        public void SetArrowUnvisible()
        {
            Control.Image = null;
        }

        public DemoFormExtension(Form parent, Button[] buttons, Button control)
        {
            Parent = parent;
            Buttons = buttons;
            Control = control;
            ButtonColors = new System.Drawing.Color[buttons.Length];
        }
    }
}

