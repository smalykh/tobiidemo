﻿namespace TobiiDemo
{
    partial class ModeSelectorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RunDemoButton = new System.Windows.Forms.Button();
            this.RunNavigationButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelInterpreter = new System.Windows.Forms.Label();
            this.radioButtonRose = new System.Windows.Forms.RadioButton();
            this.AutoButton = new System.Windows.Forms.RadioButton();
            this.FreeViewButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // RunDemoButton
            // 
            this.RunDemoButton.Location = new System.Drawing.Point(17, 127);
            this.RunDemoButton.Name = "RunDemoButton";
            this.RunDemoButton.Size = new System.Drawing.Size(240, 98);
            this.RunDemoButton.TabIndex = 0;
            this.RunDemoButton.Text = "Demo";
            this.RunDemoButton.UseVisualStyleBackColor = true;
            this.RunDemoButton.Click += new System.EventHandler(this.RunDemoButton_Click);
            // 
            // RunNavigationButton
            // 
            this.RunNavigationButton.Location = new System.Drawing.Point(17, 253);
            this.RunNavigationButton.Name = "RunNavigationButton";
            this.RunNavigationButton.Size = new System.Drawing.Size(240, 98);
            this.RunNavigationButton.TabIndex = 0;
            this.RunNavigationButton.Text = "Navigation";
            this.RunNavigationButton.UseVisualStyleBackColor = true;
            this.RunNavigationButton.Click += new System.EventHandler(this.RunNavigationButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Choose operation mode:";
            // 
            // labelInterpreter
            // 
            this.labelInterpreter.AutoSize = true;
            this.labelInterpreter.Location = new System.Drawing.Point(14, 57);
            this.labelInterpreter.Name = "labelInterpreter";
            this.labelInterpreter.Size = new System.Drawing.Size(78, 17);
            this.labelInterpreter.TabIndex = 3;
            this.labelInterpreter.Text = "Interpreter:";
            // 
            // radioButtonRose
            // 
            this.radioButtonRose.AutoSize = true;
            this.radioButtonRose.Checked = true;
            this.radioButtonRose.Location = new System.Drawing.Point(17, 77);
            this.radioButtonRose.Name = "radioButtonRose";
            this.radioButtonRose.Size = new System.Drawing.Size(62, 21);
            this.radioButtonRose.TabIndex = 4;
            this.radioButtonRose.TabStop = true;
            this.radioButtonRose.Text = "Rose";
            this.radioButtonRose.UseVisualStyleBackColor = true;
            this.radioButtonRose.CheckedChanged += new System.EventHandler(this.RadioButtonRose_CheckedChanged);
            // 
            // AutoButton
            // 
            this.AutoButton.AutoSize = true;
            this.AutoButton.Location = new System.Drawing.Point(85, 77);
            this.AutoButton.Name = "AutoButton";
            this.AutoButton.Size = new System.Drawing.Size(58, 21);
            this.AutoButton.TabIndex = 4;
            this.AutoButton.TabStop = true;
            this.AutoButton.Text = "Auto";
            this.AutoButton.UseVisualStyleBackColor = true;
            this.AutoButton.CheckedChanged += new System.EventHandler(this.RadioButton1_CheckedChanged);
            // 
            // FreeViewButton
            // 
            this.FreeViewButton.AutoSize = true;
            this.FreeViewButton.Location = new System.Drawing.Point(162, 77);
            this.FreeViewButton.Name = "FreeViewButton";
            this.FreeViewButton.Size = new System.Drawing.Size(87, 21);
            this.FreeViewButton.TabIndex = 4;
            this.FreeViewButton.Text = "FreeView";
            this.FreeViewButton.UseVisualStyleBackColor = true;
            this.FreeViewButton.CheckedChanged += new System.EventHandler(this.FreeViewButton_CheckedChanged);
            // 
            // ModeSelectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 374);
            this.Controls.Add(this.FreeViewButton);
            this.Controls.Add(this.AutoButton);
            this.Controls.Add(this.radioButtonRose);
            this.Controls.Add(this.labelInterpreter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RunNavigationButton);
            this.Controls.Add(this.RunDemoButton);
            this.Name = "ModeSelectorForm";
            this.Text = "ModeSelectorForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button RunDemoButton;
        private System.Windows.Forms.Button RunNavigationButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelInterpreter;
        private System.Windows.Forms.RadioButton radioButtonRose;
        private System.Windows.Forms.RadioButton AutoButton;
        private System.Windows.Forms.RadioButton FreeViewButton;
    }
}