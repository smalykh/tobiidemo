﻿using System.Windows;
using System;
using System.Windows.Forms;
namespace TobiiDemo
{
    partial class DemoFormRose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.buttonDown = new System.Windows.Forms.Button();
            this.tableLayoutPanelMiddle = new System.Windows.Forms.TableLayoutPanel();
            this.buttonRight = new System.Windows.Forms.Button();
            this.buttonCenter = new System.Windows.Forms.Button();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.tableLayoutPanelUp = new System.Windows.Forms.TableLayoutPanel();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonUpLeft = new System.Windows.Forms.Button();
            this.buttonUpRight = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.tableLayoutPanelMiddle.SuspendLayout();
            this.tableLayoutPanelUp.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button1.Location = new System.Drawing.Point(0, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "Turn left";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button2.Location = new System.Drawing.Point(720, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 27);
            this.button2.TabIndex = 7;
            this.button2.Text = "Turn rigth";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Controls.Add(this.buttonDown, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.tableLayoutPanelMiddle, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.tableLayoutPanelUp, 0, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel.TabIndex = 10;
            // 
            // buttonDown
            // 
            this.buttonDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDown.Location = new System.Drawing.Point(3, 318);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(794, 129);
            this.buttonDown.TabIndex = 2;
            this.buttonDown.Text = "Forward Right";
            this.buttonDown.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelMiddle
            // 
            this.tableLayoutPanelMiddle.ColumnCount = 3;
            this.tableLayoutPanelMiddle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMiddle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelMiddle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMiddle.Controls.Add(this.buttonRight, 0, 0);
            this.tableLayoutPanelMiddle.Controls.Add(this.buttonCenter, 0, 0);
            this.tableLayoutPanelMiddle.Controls.Add(this.buttonLeft, 0, 0);
            this.tableLayoutPanelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMiddle.Location = new System.Drawing.Point(3, 93);
            this.tableLayoutPanelMiddle.Name = "tableLayoutPanelMiddle";
            this.tableLayoutPanelMiddle.RowCount = 1;
            this.tableLayoutPanelMiddle.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMiddle.Size = new System.Drawing.Size(794, 219);
            this.tableLayoutPanelMiddle.TabIndex = 1;
            // 
            // buttonRight
            // 
            this.buttonRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRight.Location = new System.Drawing.Point(717, 3);
            this.buttonRight.Name = "buttonRight";
            this.buttonRight.Size = new System.Drawing.Size(74, 213);
            this.buttonRight.TabIndex = 3;
            this.buttonRight.Text = "Forward Right";
            this.buttonRight.UseVisualStyleBackColor = true;
            // 
            // buttonCenter
            // 
            this.buttonCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCenter.Location = new System.Drawing.Point(82, 3);
            this.buttonCenter.Name = "buttonCenter";
            this.buttonCenter.Size = new System.Drawing.Size(629, 213);
            this.buttonCenter.TabIndex = 2;
            this.buttonCenter.Text = "Forward Right";
            this.buttonCenter.UseVisualStyleBackColor = true;
            // 
            // buttonLeft
            // 
            this.buttonLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonLeft.Location = new System.Drawing.Point(3, 3);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(73, 213);
            this.buttonLeft.TabIndex = 1;
            this.buttonLeft.Text = "Forward Right";
            this.buttonLeft.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelUp
            // 
            this.tableLayoutPanelUp.ColumnCount = 3;
            this.tableLayoutPanelUp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelUp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelUp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelUp.Controls.Add(this.buttonUp, 1, 0);
            this.tableLayoutPanelUp.Controls.Add(this.buttonUpLeft, 0, 0);
            this.tableLayoutPanelUp.Controls.Add(this.buttonUpRight, 2, 0);
            this.tableLayoutPanelUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelUp.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelUp.Name = "tableLayoutPanelUp";
            this.tableLayoutPanelUp.RowCount = 1;
            this.tableLayoutPanelUp.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelUp.Size = new System.Drawing.Size(794, 84);
            this.tableLayoutPanelUp.TabIndex = 0;
            // 
            // buttonUp
            // 
            this.buttonUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUp.Location = new System.Drawing.Point(82, 3);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(629, 78);
            this.buttonUp.TabIndex = 2;
            this.buttonUp.Text = "Forward Right";
            this.buttonUp.UseVisualStyleBackColor = true;
            // 
            // buttonUpLeft
            // 
            this.buttonUpLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUpLeft.Location = new System.Drawing.Point(3, 3);
            this.buttonUpLeft.Name = "buttonUpLeft";
            this.buttonUpLeft.Size = new System.Drawing.Size(73, 78);
            this.buttonUpLeft.TabIndex = 1;
            this.buttonUpLeft.Text = "Forward Right";
            this.buttonUpLeft.UseVisualStyleBackColor = true;
            // 
            // buttonUpRight
            // 
            this.buttonUpRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUpRight.Location = new System.Drawing.Point(717, 3);
            this.buttonUpRight.Name = "buttonUpRight";
            this.buttonUpRight.Size = new System.Drawing.Size(74, 78);
            this.buttonUpRight.TabIndex = 0;
            this.buttonUpRight.Text = "Forward Right";
            this.buttonUpRight.UseVisualStyleBackColor = true;
            // 
            // DemoFormRose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "DemoFormRose";
            this.Text = "DemoForm";
            this.Load += new System.EventHandler(this.DemoForm_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanelMiddle.ResumeLayout(false);
            this.tableLayoutPanelUp.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button button1;
        private Button button2;
        private TableLayoutPanel tableLayoutPanel;
        private Button buttonDown;
        private TableLayoutPanel tableLayoutPanelMiddle;
        private Button buttonRight;
        private Button buttonCenter;
        private Button buttonLeft;
        private TableLayoutPanel tableLayoutPanelUp;
        private Button buttonUp;
        private Button buttonUpLeft;
        private Button buttonUpRight;
    }
}