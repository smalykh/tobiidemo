﻿using System.Windows;
using System;
using System.Windows.Forms;
namespace TobiiDemo
{
    partial class DemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        const int ButtonsNum = 5;
        private Button[] Buttons;

        private string SystemOn = "Navifation mode is off";
        private string DemoMsg = "";


        private System.Drawing.Color[] ButtonColors = new System.Drawing.Color[ButtonsNum];
        public void OnUserAction(object sender, System.EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(delegate
                {
                    OnUserAction(sender, e);
                }));
            }
            else
            { 
                DemoEngine.DemoEngineState state = (DemoEngine.DemoEngineState)sender;

                for (int i = 0; i < ButtonsNum; i++)
                    ButtonColors[i] = System.Drawing.Color.Empty;

                switch (state)
                {
                    case DemoEngine.DemoEngineState.Started:
                        ButtonColors[1] = System.Drawing.Color.Violet;
                        break;
                    case DemoEngine.DemoEngineState.Stopped:
                        ButtonColors[2] = System.Drawing.Color.Violet;
                        break;
                    case DemoEngine.DemoEngineState.Forward:
                        ButtonColors[0] = System.Drawing.Color.Violet;
                        break;
                    case DemoEngine.DemoEngineState.Left:
                        ButtonColors[3] = System.Drawing.Color.Violet;
                        break;
                    case DemoEngine.DemoEngineState.Rigth:
                        ButtonColors[4] = System.Drawing.Color.Violet;
                        break;
                }
                DemoMsg = ((EventArgsDemo)e).Message;

                UpdateInformationText();
                ResetButtonsColor();
            }
        }

        public void OnStateChanged(object sender, System.EventArgs e)
        {

            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(delegate
                {
                    OnStateChanged(sender, e);
                }));
            }
            else
            {
                NavigationSystem.NavigationSystemState State = (NavigationSystem.NavigationSystemState)sender;
                if (State == NavigationSystem.NavigationSystemState.SystemStarted)
                {
                    SystemOn = "Navifation mode is on";
                }
                else
                {
                    SystemOn = "Navifation mode is off";
                }
                UpdateInformationText();
            }
        }
        public void OnMoveCommandReceived(object sender, System.EventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                CommandAdapter.MoveCommandType cmd = (CommandAdapter.MoveCommandType)sender;
                switch (cmd)
                {
                    case CommandAdapter.MoveCommandType.ForwardCommand:
                        SetForwardArrowVisible();
                        break;
                    case CommandAdapter.MoveCommandType.TurnLeftCommand:
                        SetLeftArrowVisible();
                        break;
                    case CommandAdapter.MoveCommandType.TurnRigthCommand:
                        SetRigthArrowVisible();
                        break;
                    default:
                        SetArrowUnvisible();
                        break;
                }
            }));
        }
        public void OnCommandReceived(object sender, System.EventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                NavigationSystem.GazeDirection Direction = (NavigationSystem.GazeDirection)sender;
                ResetButtonsColor();
                switch (Direction)
                {
                    case NavigationSystem.GazeDirection.DirectionDown:
                        this.buttonDown.BackColor = System.Drawing.Color.Yellow;
                        break;
                    case NavigationSystem.GazeDirection.DirectionUp:
                        this.buttonUp.BackColor = System.Drawing.Color.Yellow;
                        break;
                    case NavigationSystem.GazeDirection.DirectionForward:
                        this.buttonCenter.BackColor = System.Drawing.Color.Yellow;
                        break;
                    case NavigationSystem.GazeDirection.DirectionLeft:
                        this.buttonLeft.BackColor = System.Drawing.Color.Yellow;
                        break;
                    case NavigationSystem.GazeDirection.DirectionRigth:
                        this.buttonRigth.BackColor = System.Drawing.Color.Yellow;
                        break;
                    default:
                        throw new System.Exception();
                }
            }));
        }

        public void CustomizeDemoForm(int thX, int thY)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;

            this.buttonUp.MinimumSize = new System.Drawing.Size(0, thY);
            this.buttonDown.MinimumSize = new System.Drawing.Size(0, thY);
            this.buttonLeft.MinimumSize = new System.Drawing.Size(thX, 0);
            this.buttonRigth.MinimumSize = new System.Drawing.Size(thX, 0);
        }

        private void ResetButtonsColor()
        {
            for (int i = 0; i < ButtonsNum; i++)
                Buttons[i].BackColor = ButtonColors[i];

            //this.buttonCenter.BackColor = new System.Drawing.Color();
            //this.buttonUp.BackColor = new System.Drawing.Color();
            //this.buttonDown.BackColor = new System.Drawing.Color();
            //this.buttonLeft.BackColor = new System.Drawing.Color();
            //this.buttonRigth.BackColor = new System.Drawing.Color();
        }

        public void UpdateInformationText()
        {
            this.buttonCenter.Text = DemoMsg + "\n\n" + SystemOn;
        }

        public void SetForwardArrowVisible()
        {
            this.buttonCenter.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_fwd.png"));
        }
        public void SetLeftArrowVisible()
        {
            this.buttonCenter.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_l.png"));
        }
        public void SetRigthArrowVisible()
        {
            this.buttonCenter.Image = System.Drawing.Image.FromFile(System.IO.Path.GetFullPath("..\\..\\..\\arrow_r.png"));
        }
        public void SetArrowUnvisible()
        {
            this.buttonCenter.Image = null;
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoForm));
            this.label1 = new System.Windows.Forms.Label();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.buttonRigth = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonCenter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(71, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // buttonUp
            // 
            this.buttonUp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonUp.Location = new System.Drawing.Point(71, 0);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(649, 23);
            this.buttonUp.TabIndex = 1;
            this.buttonUp.Text = "Start navigation";
            this.buttonUp.UseVisualStyleBackColor = true;
            // 
            // buttonLeft
            // 
            this.buttonLeft.AutoSize = true;
            this.buttonLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonLeft.Location = new System.Drawing.Point(0, 0);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(71, 450);
            this.buttonLeft.TabIndex = 2;
            this.buttonLeft.Text = "Turn left";
            this.buttonLeft.UseVisualStyleBackColor = true;
            // 
            // buttonRigth
            // 
            this.buttonRigth.AutoSize = true;
            this.buttonRigth.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonRigth.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonRigth.Location = new System.Drawing.Point(720, 0);
            this.buttonRigth.Name = "buttonRigth";
            this.buttonRigth.Size = new System.Drawing.Size(80, 450);
            this.buttonRigth.TabIndex = 3;
            this.buttonRigth.Text = "Turn rigth";
            this.buttonRigth.UseVisualStyleBackColor = true;
            // 
            // buttonDown
            // 
            this.buttonDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonDown.Location = new System.Drawing.Point(71, 427);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(649, 23);
            this.buttonDown.TabIndex = 4;
            this.buttonDown.Text = "Stop Navigation";
            this.buttonDown.UseVisualStyleBackColor = true;
            // 
            // buttonCenter
            // 
            this.buttonCenter.AutoSize = true;
            this.buttonCenter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCenter.Image = ((System.Drawing.Image)(resources.GetObject("buttonCenter.Image")));
            this.buttonCenter.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonCenter.Location = new System.Drawing.Point(71, 23);
            this.buttonCenter.Name = "buttonCenter";
            this.buttonCenter.Size = new System.Drawing.Size(649, 404);
            this.buttonCenter.TabIndex = 5;
            this.buttonCenter.Text = "Move forward";
            this.buttonCenter.UseVisualStyleBackColor = true;
            // 
            // DemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonCenter);
            this.Controls.Add(this.buttonDown);
            this.Controls.Add(this.buttonUp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonRigth);
            this.Controls.Add(this.buttonLeft);
            this.Name = "DemoForm";
            this.Text = "DemoForm";
            this.Load += new System.EventHandler(this.DemoForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonLeft;
        private System.Windows.Forms.Button buttonRigth;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonCenter;
    }
}